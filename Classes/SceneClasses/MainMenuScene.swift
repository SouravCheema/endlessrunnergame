//
//  MainMenuScene.swift
//  tRexRunner
//
//  Created by Sourav Dewett on 2019-02-26.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import Foundation
import SpriteKit

class MainMenuScene : SKScene {
    
    var playBtn = SKSpriteNode()
    var scoreBtn = SKSpriteNode()
    var title = SKLabelNode()
    var scoreLabel = SKLabelNode()
    
    func initialize() {
        createBg()
        createGrounds()
        getButtons()
        getLabel()
    }
    
    override func didMove(to view: SKView) {
        self.initialize()
    }
    
    override func update(_ currentTime: TimeInterval) {
        moveBackGroundsAndGrounds()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            if let touch = touches.first {
                let position = touch.location(in: self)
                let spriteTouched = self.atPoint(position)
                if(spriteTouched == self.playBtn) {
                    let gameplay = GamePlayScene(fileNamed: "GamePlayScene")
                    gameplay?.scaleMode = .aspectFill
                    self.view?.presentScene(gameplay!, transition: SKTransition.doorway(withDuration: 2))
                }
                
                if(spriteTouched == self.scoreBtn) {
                    showScore()
                }
        }
    }

    

    
    
    func createBg() {
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "BG")
            bg.name = "BG"
            bg.anchorPoint = CGPoint(x:0.5, y:0.5)
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: 0)
            bg.zPosition = 0
            
            self.addChild(bg)
        }
    }
    
    func createGrounds() {
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "Ground")
            bg.name = "Ground"
            bg.anchorPoint = CGPoint(x:0.5, y:0.5)
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: -(self.size.height/2))
            bg.zPosition = 3
            self.addChild(bg)
            
            
            
        }
    }
    
    func moveBackGroundsAndGrounds() {
        enumerateChildNodes(withName: "BG", using: ({(node, error) in
            node.position.x -= 4
            
            if( node.position.x < -(self.frame.width)) {
                node.position.x += self.frame.width * 3
            }
        }) )
        
        enumerateChildNodes(withName: "Ground", using: ({(node, error) in
            node.position.x -= 7
            
            if( node.position.x < -(self.frame.width)) {
                node.position.x += self.frame.width * 3
            }
        }) )
    }
    
    func getButtons() {
        playBtn = self.childNode(withName: "Play") as! SKSpriteNode
        scoreBtn = self.childNode(withName: "highScore") as! SKSpriteNode
        
    }
    
    func getLabel() {
        title = self.childNode(withName: "Title") as! SKLabelNode
        
        title.fontSize = 120
        title.text = "TREX RUNNER"
        title.zPosition = 5
        
        let moveUp = SKAction.moveTo(y: title.position.y + 50, duration: 1.3)
        let moveDown = SKAction.moveTo(y: title.position.y - 50, duration: 1.3)
        let sequence = SKAction.sequence([moveUp,moveDown])
        
        title.run(SKAction.repeatForever(sequence))
    }
    
    func showScore() {
        scoreLabel.removeFromParent()
        scoreLabel = SKLabelNode(fontNamed: "Rockwell")
        scoreLabel.fontSize = 60
        scoreLabel.text = "\(UserDefaults.standard.integer(forKey: "highScore"))"
        scoreLabel.position = CGPoint(x: 0, y: -200)
        scoreLabel.zPosition = 9
        
        self.addChild(scoreLabel)
    }
}
