//
//  GamePlayScene.swift
//  tRexRunner
//
//  Created by Sourav Dewett on 2019-02-24.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import Foundation
import SpriteKit

class GamePlayScene : SKScene, SKPhysicsContactDelegate {
    
    var player = Player()
    var canJump = false
    var obstacles = [SKSpriteNode]()
    var movePlayer = false
    var playerOnObstacle = false
    var isAlive = false
    var spawner = Timer()
    var counter = Timer()
    var scoreLabel = SKLabelNode()
    var score : Int = 0
    var youWinLabel = SKLabelNode()
    var pausePanel = SKSpriteNode()
    let emoji = "😄"
    
    override func didMove(to view: SKView) {
        initiazlize()
    }
    
    override func update(_ currentTime: TimeInterval) {
        if(isAlive) {
        moveBackGroundsAndGrounds()
        }
        if(movePlayer) {
            player.position.x -= 9
        }
        
        checkPlayerBounds()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            let position = touch.location(in: self)
            let spriteTouched = self.atPoint(position)
            
            if(spriteTouched.name == "Restart") {
                let gameplay = GamePlayScene(fileNamed: "GamePlayScene")
                gameplay?.scaleMode = .aspectFill
                self.view?.presentScene(gameplay!, transition: SKTransition.doorway(withDuration: 1.5))
            }
            
            if(spriteTouched.name == "Quit") {
                let gameplay = MainMenuScene(fileNamed: "MainMenuScene")
                gameplay?.scaleMode = .aspectFill
                self.view?.presentScene(gameplay!, transition: SKTransition.doorway(withDuration: 1.5))
            }
            
            if(spriteTouched.name == "pause") {
                createPausePanel()
            }
            
            if(spriteTouched.name == "Resume") {
                pausePanel.removeFromParent()
                self.scene?.isPaused = false
                spawner = Timer.scheduledTimer(timeInterval: (TimeInterval(randomBetweenNumbers(firstNumber: 2.5, secondNumber: 6))), target: self, selector: #selector(GamePlayScene.spawnObstacles), userInfo: nil, repeats: true)
                
                counter = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GamePlayScene.getScoreLabel), userInfo: nil, repeats: true)
                
            }
            
            if(spriteTouched.name == "Quit") {
                let gameplay = MainMenuScene(fileNamed: "MainMenuScene")
                gameplay?.scaleMode = .aspectFill
                self.view?.presentScene(gameplay!, transition: SKTransition.doorway(withDuration: 1.5))
            }


        }
        
        if (canJump == true) {
            canJump = false
            player.jump();
            run(player.playJumpSound())
        }
        
        if(playerOnObstacle) {
            player.jump()
            run(player.playJumpSound())
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody = SKPhysicsBody()
        var secondBody = SKPhysicsBody()
        
        if(contact.bodyA.node?.name == "Player") {
            firstBody = contact.bodyA;
            secondBody = contact.bodyB;
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if(firstBody.node?.name == "Player" && (secondBody.node?.name)! == "Ground") {
            canJump = true
        }
        
        if(firstBody.node?.name == "Player" && (secondBody.node?.name)! == "Obstacle") {
            if(!canJump) {
                movePlayer = true
                playerOnObstacle = true
            }
            
        }
        
        if(firstBody.node?.name == "Player" && (secondBody.node?.name)! == "Cactus") {
            playerDied()
        }
        
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        var firstBody = SKPhysicsBody()
        var secondBody = SKPhysicsBody()
        
        if(contact.bodyA.node?.name == "Player") {
            firstBody = contact.bodyA;
            secondBody = contact.bodyB;
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if(firstBody.node?.name == "Player" && (secondBody.node?.name)! == "Obstacle") {
                movePlayer = false
                playerOnObstacle = false
        }
    }
    
    func initiazlize(){
        physicsWorld.contactDelegate = self
        isAlive = true
        createPlayer()
        createBg()
        createGrounds()
        createObstacles()
        
        
        
//        Timer.scheduledTimer(timeInterval: 2, target: self, selector: Selector(("spawnObstacles")), userInfo: nil, repeats: true)
        
        spawner = Timer.scheduledTimer(timeInterval: (TimeInterval(randomBetweenNumbers(firstNumber: 2.5, secondNumber: 6))), target: self, selector: #selector(GamePlayScene.spawnObstacles), userInfo: nil, repeats: true)
        
        counter = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GamePlayScene.getScoreLabel), userInfo: nil, repeats: true)
        
        
//        self.run(SKAction.repeatForever(playBGMusic()))
    }
    
    func createPlayer() {
        player = Player(imageNamed: "Player 1")
        player.initialize()
        player.position = CGPoint(x: -10, y: 20)
        self.addChild(player)
    }
    
    func createBg() {
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "BG")
            bg.name = "BG"
            bg.anchorPoint = CGPoint(x:0.5, y:0.5)
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: 0)
            bg.zPosition = 0
            
            self.addChild(bg)
        }
    }
    
    func createGrounds() {
        for i in 0...2 {
            let bg = SKSpriteNode(imageNamed: "Ground")
            bg.name = "Ground"
            bg.anchorPoint = CGPoint(x:0.5, y:0.5)
            bg.position = CGPoint(x: CGFloat(i) * bg.size.width, y: -(self.size.height/2))
            bg.zPosition = 3
            bg.physicsBody = SKPhysicsBody(rectangleOf: bg.size)
            bg.physicsBody?.affectedByGravity = false
            bg.physicsBody?.isDynamic = false
            bg.physicsBody?.categoryBitMask = ColliderType.ground
            self.addChild(bg)
            
            
            
        }
    }
    
    func moveBackGroundsAndGrounds() {
        enumerateChildNodes(withName: "BG", using: ({(node, error) in
            node.position.x -= 4
            
            if( node.position.x < -(self.frame.width)) {
                node.position.x += self.frame.width * 3
            }
        }) )
        
        enumerateChildNodes(withName: "Ground", using: ({(node, error) in
            node.position.x -= 7
            
            if( node.position.x < -(self.frame.width)) {
                node.position.x += self.frame.width * 3
            }
        }) )
    }
    
    func createObstacles() {
        for i in 0...5 {
            
            
            let obstacle = SKSpriteNode(imageNamed: "Obstacle \(i)")
            if(i == 0){
                obstacle.name = "Cactus"
                obstacle.setScale(0.4)
            } else {
                obstacle.name = "Obstacle"
                obstacle.setScale(0.5)
            }
            //obstacle.name = "Obstacle"
            //obstacle.setScale(0.5)
            obstacle.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            obstacle.zPosition = 1
            
            obstacle.physicsBody = SKPhysicsBody(rectangleOf: obstacle.size)
            obstacle.physicsBody?.allowsRotation = false
            obstacle.physicsBody?.categoryBitMask = ColliderType.obstacle
            
            obstacles.append(obstacle)
        }
    }
    
    @objc func spawnObstacles() {
        let index = Int(arc4random_uniform(UInt32(obstacles.count)))
        
        let obstacle = obstacles[index].copy() as! SKSpriteNode
        
        obstacle.position = CGPoint(x: self.frame.width + obstacle.size.width, y: 50)
        
        let move = SKAction.moveTo(x: -(self.frame.size.width * 2), duration: 15)
        
        let remove = SKAction.removeFromParent()
        
        let sequence = SKAction.sequence([move,remove])
        
        obstacle.run(sequence)
        
        self.addChild(obstacle)
    }
    
    func randomBetweenNumbers(firstNumber:CGFloat, secondNumber: CGFloat) -> CGFloat {
        
        return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(firstNumber - secondNumber) + min(firstNumber,secondNumber)
    }
    
    func checkPlayerBounds() {
        if(isAlive){
            if(player.position.x < -((self.frame.size.width) / 2) - 35 ) {
            playerDied()
            }
        }
    }
    
    func playerDied() {
        
        let highScore = UserDefaults.standard.integer(forKey: "highScore")
        
        if(highScore < score) {
            UserDefaults.standard.set(score, forKey: "highScore")
        }
        
        player.removeFromParent()
        
        for child in children {
            if (child.name == "Obstacle" || child.name == "Cactus") {
                child.removeFromParent()
            }
        }
        
        isAlive = false
        spawner.invalidate()
        counter.invalidate()
        let  restart = SKSpriteNode(imageNamed: "Restart")
        let quit = SKSpriteNode(imageNamed: "Quit")
        
        restart.name = "Restart"
        restart.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        restart.position = CGPoint(x: -200, y: -150)
        restart.zPosition = 10
        restart.setScale(0)
        
        quit.name = "Quit"
        quit.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        quit.position = CGPoint(x: 200, y: -150)
        quit.zPosition = 10
        quit.setScale(0)
        
        let scaleUp = SKAction.scale(to: 1, duration: 0.5)
        restart.run(scaleUp)
        quit.run(scaleUp)
        self.addChild(restart)
        self.addChild(quit)
    }
    
    func createPausePanel(){
        
        
        spawner.invalidate()
        counter.invalidate()
        self.scene?.isPaused = true
        pausePanel = SKSpriteNode(imageNamed: "Pause Panel")
        pausePanel.anchorPoint = CGPoint(x:0.5,y:0.5)
        pausePanel.position = CGPoint(x: 0, y: 0)
        pausePanel.zPosition = 10
        
        let resume = SKSpriteNode(imageNamed: "Play")
        let quit = SKSpriteNode(imageNamed: "Quit")
        
        resume.name = "Resume"
        resume.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        resume.position = CGPoint(x:-155,y:0)
        resume.setScale(0.75)
        
        quit.name = "Quit"
        quit.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        quit.position = CGPoint(x:155,y:0)
        quit.setScale(0.75)
        
        pausePanel.addChild(resume)
        pausePanel.addChild(quit)
        
        self.addChild(pausePanel)
        
        
        
    }
    
    @objc func getScoreLabel() {
        scoreLabel = self.childNode(withName: "scoreLabel") as! SKLabelNode
        scoreLabel.text = "0"
        
        score = score + 1;
        scoreLabel.text = "\(score)"
        
        if(score == 100) {
            youWinLabel = self.childNode(withName: "YouWin") as! SKLabelNode
            youWinLabel.fontSize = 70
            youWinLabel.text = "You Won \(emoji) "
            youWinLabel.zPosition = 6
            youWinLabel.isHidden = false
            youWinLabel.position = CGPoint(x: 0.5, y: 0.5)
            playerDied()
            
        }
    }
    
    func playBGMusic() -> SKAction {
        let bgSound = SKAction.playSoundFileNamed("bgSound.mp3", waitForCompletion: false)
        
        return bgSound
    }
    
}
