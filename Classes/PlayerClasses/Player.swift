//
//  Player.swift
//  tRexRunner
//
//  Created by Sourav Dewett on 2019-02-26.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import Foundation
import SpriteKit

struct ColliderType {
    static let player :UInt32 = 1
    static let ground :UInt32 = 2
    static let obstacle :UInt32 = 3
}

class Player : SKSpriteNode {
    func initialize() {
        
        let action = createWalkingPlayerAnimation()
        
        
        self.run(SKAction.repeatForever(action))
        
//        self.run(SKAction.repeatForever(playBGMusic()))
        
        self.name = "Player"
        self.zPosition = 2
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.setScale(0.5)
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody?.affectedByGravity = true
        self.physicsBody?.allowsRotation = false
        self.physicsBody?.categoryBitMask = ColliderType.player
        self.physicsBody?.collisionBitMask = ColliderType.ground | ColliderType.obstacle
        self.physicsBody?.contactTestBitMask = ColliderType.ground | ColliderType.obstacle
        
        
    }
    
    func jump() {
       
        self.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        self.physicsBody?.applyImpulse(CGVector(dx: 100,dy: 300))
    }
    
    func createWalkingPlayerAnimation() -> SKAction{
        //animate the cat going to sleep
        let frame1 = SKTexture.init(imageNamed: "Player 1")
        let frame2 = SKTexture.init(imageNamed: "Player 2")
        let frame3 = SKTexture.init(imageNamed: "Player 3")
        let frame4 = SKTexture.init(imageNamed: "Player 4")
        let frame5 = SKTexture.init(imageNamed: "Player 5")
        let frame6 = SKTexture.init(imageNamed: "Player 6")
        let frame7 = SKTexture.init(imageNamed: "Player 7")
        let frame8 = SKTexture.init(imageNamed: "Player 8")
        let frame9 = SKTexture.init(imageNamed: "Player 9")
        let frame10 = SKTexture.init(imageNamed: "Player 10")
        let frame11 = SKTexture.init(imageNamed: "Player 11")


        let sleepFrames: [SKTexture] = [frame1, frame2,frame3,frame4,frame5,frame6, frame7,frame8,frame9,frame10,frame11]
        
        // Change the frame per 0.25 sec
        let animation = SKAction.animate(with: sleepFrames, timePerFrame: 0.03)
        return animation
    }
    
    func playJumpSound() -> SKAction {
        let jumpSound = SKAction.playSoundFileNamed("Jump.wav", waitForCompletion: false)
        
        return jumpSound
    }
    
    
    
}


